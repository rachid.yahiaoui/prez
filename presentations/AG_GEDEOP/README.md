# AG GEDEOP

## Demo ( /home/ryahiaoui/Téléchargements/AG_CATI_GEDEOP_SEMGRAH_DEMO/ )

#### **1. Décrire en RDF les vols ( ayant eu un incident/accident ) par leur numéro de vols et le pays qui les a opéré ( 0_flight_description.json )**

  **1.1- URI Concept 1 :**  

        http://aero/flight/{flight_number}-{flight_date}

   **1.2- Typer la ressource :**

        Avec le concept http://aviation.owl#Flight en le récupérant depuis l’ontologie

   **1.3- Les littéraux :**

       "{flight_number}"  
        ⇒ Prédicat associé :   http://aviation.owl#HasFlightNumber

       "{flight_operator_country}"  
        ⇒ Prédicat associé :   http://aviation.owl#FlightOperatedByCountry

   **1.4- SQL :**
```
     SELECT flight_number           AS "flight_number" ,
            flight_date             AS "flight_date"   ,
            flight_operator_country AS "flight_operator_country" 
     FROM   accident
 ```

   **1.5- SPARQL-Client-Filter :**

```
     CONSTRUCT {
        ?flight  <http://aviation.owl#HasFlightNumber>         ?flightNumber .  
        ?flight  <http://aviation.owl#FlightOperatedByCountry> ?flightOperatorCountry .    
     }
     WHERE {
        ?flight  <http://aviation.owl#HasFlightNumber>         ?flightNumber .  
        ?flight  <http://aviation.owl#FlightOperatedByCountry> ?flightOperatorCountry .    
     }
```

   **1.6- DB Connection :**

```
     ryahiaoui    #    yahiaoui    #     jdbc:postgresql://127.0.0.1:5432/crash
```

   **1.7- Push to ( backend ) Coby**

   **1.8- Goto : https://localhost:8585/** 
```
    SI = flight & CLASS = flight
```
---

#### **2. Calculer le nombre total de décédés dans des accidents d'avion, par pays, pour toutes les années et les classer par ordre décroissant ( 1-aircraft-accident.zip )**


   **2.1- Importer l’archive : 1-aircraft-accident.zip**


   **2.2- Push to Coby**
```
     SI = aircraft & CLASS = accident & accident_year = 1919_2022
```

   **2.3- Nombre de décédés dans des accidents d’avions par pays et par année :**

```
     SELECT ?flightOperatorCountry  ?accidentTotalFatalities ?accidentYear {
     
       ?flight   <http://aviation.owl#FlightOperatedByCountry> ?flightOperatorCountry .
       ?flight   <http://aviation.owl#FlightEmploysAnAircraft> ?aircraft .
       ?flight   <http://aviation.owl#FlightHadAccident>       ?accident .  
       ?accident <http://aviation.owl#AccidentYear>            ?accidentYear .  
       ?accident <http://aviation.owl#AccidentTotalFatalities> ?accidentTotalFatalities .  
       # FILTER  ( ?accidentTotalFatalities > 0 ) .
       # FILTER  ( ?flightOperatorCountry  = 'France' ) .
       # FILTER  ( ?accidentYear           = 2001 ) .
     }
     ORDER BY DESC( ?accidentTotalFatalities )
```

   **3.3- Nombre total de décédés dans des accidents d’avions pour CHAQUE PAYS et pour TOUTES LES ANNÉES :**

```
     SELECT   ?flightOperatorCountry 
            ( COUNT( ?accidentTotalFatalities) AS ?NbAccident      ) 
            ( SUM(?accidentTotalFatalities)    AS ?TotalFatalities ) {
     
        ?flight   <http://aviation.owl#FlightOperatedByCountry> ?flightOperatorCountry .
        ?flight   <http://aviation.owl#FlightEmploysAnAircraft> ?aircraft .
        ?flight   <http://aviation.owl#FlightHadAccident>       ?accident .  
        ?accident <http://aviation.owl#AccidentYear>            ?accidentYear .  
        ?accident <http://aviation.owl#AccidentTotalFatalities> ?accidentTotalFatalities .  
        # FILTER  ( ?accidentTotalFatalities > 0 ) .
        # FILTER  ( ?accidentYear = 2001 ) .
        # FILTER  ( ?flightOperatorCountry = 'France' ) .         
     }
     GROUP BY ( ?flightOperatorCountry )
     ORDER BY DESC( ?TotalFatalities )
```



